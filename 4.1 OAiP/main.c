/*Реализовать функцию поиска максимального четного значения,
среди переданных в качестве параметров целых чисел.*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
int search(int n,...);
int main()
{
    int n,mmax;
    mmax=search(10, 2,1,-9,-10,5,6,8,7,100,1000);
    printf("Максимальное четное число = %d\n",mmax);
    return 0;
}
int search(int n,...){
    int i,max=0,temp;
    va_list ap;
    va_start(ap,n);
    for(i=1;i<=n;i++){
        temp=va_arg(ap,int);
      //  printf("%d ",temp);
        if(temp>max && (temp%2)==0){
            //printf("1");
            max=temp;
            }
    }
    va_end(ap);
    //printf("%d\n",max);
    return max;
}
