/*Найти значение вещественного массива, наиболее близкое к заданному значению K.
Функцию поиска значения реализовать рекурсивно.*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double *Input(int n);
void Output(double *a, int n);
int search(double *a,int n, double k);
int searc(double *a, int n);
int main()
{
    srand(time(NULL));
    int n,numRes;
    double k,diff;
    double Temp_diff=1.7E+308;
    printf("Введите размер массива : ");
    scanf("%d",&n);
    double *a=Input(n);
    Output(a,n);
    printf("Введите K : ");
    scanf("%lf",&k);
   /* for(int i=0; i<n; i++)
    {
        diff=a[i]-k;
        if(diff<0)
            diff*=-1;
        if(diff<Temp_diff)
        {
            Temp_diff=diff;
            numRes=i;
        }
    }*/
    numRes = search(a,n, k);
    printf("%lf",a[numRes]);
    free(a);
    return 0;
}
double *Input(int n){
    int i;
    double *a=(double*)calloc(n,sizeof(double));
    for(i=0;i<n;i++)
        a[i]=rand()/100000000.0;
    return a;
}
void Output(double* a, int n){
    int i;
    for(i=0;i<n;i++)
        printf("%.4lf ",a[i]);
    printf("\n");
}

int search(double *a, int n, double k){
    int numRes;
    double diff;
    double difff;
   // double Temp_diff=1.7E+308;
    if(n<1)return -1;
    numRes = search(a, n-1, k);
    diff = (numRes==-1)?1.7E+308:a[numRes]-k;
    diff = (diff>0)?diff:-diff;
    difff = a[n-1]-k;
    difff = (difff>0)?difff:-difff;
    return (diff<difff)?numRes:n-1;
/*    diff=a[i]-k;
    if(diff<0)
        diff*=-1;
    if(diff<Temp_diff)
    {
        Temp_diff=diff;
        numRes=n;
    }*/
   // return search(a,n-1,k);
}
