/*Дан вещественный массив. Упорядочить массив тремя способами: в порядке возрастания
значений элементов; в порядке убывания дробной части элементов; в порядке возрастания
суммы первых четырех разрядов дробной части элементов. В функцию сортировки
должен передаваться указатель на функцию сравнения двух элементов. Способ
сортировки выбирает пользователь.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
double *Input(int n);
void swap(double *c, double *d);
void sort(double *a, int (*compare)(double, double), int n);
void Output(double *a, int n);
int compare1(double a, double b);
int compare2(double a, double b);
int compare3(double a, double b);
int main()
{
    srand(time(NULL));
    int n,cas;
    printf("Введите размер массива : ");
    scanf("%d",&n);
    double *a=Input(n);
    Output(a,n);
       do{
        printf("Выберите сортировку  :\n1-в порядке возрастания значений элементов\n");
        printf("2-в порядке убывания дробной части элементов\n");
        printf("3-в порядке возрастания суммы первых четырех разрядов дробной части элементов\n");
        scanf("%d",&cas);
    }while((cas<1||cas>3));
    switch(cas){
    case 1 : sort(a,compare1,n);break;
    case 2 : sort(a,compare2,n);break;
    case 3 : sort(a,compare3,n);break;
    }
    Output(a,n);
    free(a);
    printf("Hello world!\n");
    return 0;
}
double *Input(int n){
    int i;
    double *a=(double*)calloc(n,sizeof(double));
    for(i=0;i<n;i++)
        a[i]=rand()/100000000.0;
    return a;
}
void Output(double* a, int n){
    int i;
    for(i=0;i<n;i++)
        printf("%.4lf ",a[i]);
    printf("\n");
}
void swap(double *c, double *d){
    double temp;
    temp=*c;
    *c=*d;
    *d=temp;

}
void sort(double* a,int (*compare)(double, double), int n){
    int i,j;
    for(i=0;i<n;i++){
        for(j=0;j<n-1;j++){
            if(compare(a[j], a[j + 1]) > 0)
                swap(&a[j],&a[j+1]);
        }
    }
}
int compare1(double a, double b){
    return a>b ? 1:-1;
}
int compare2(double a, double b){
    return (a-(int)a)<(b-(int)b) ? 1:-1;
}
int compare3(double a, double b){
   double sum1=0,sum2=0;
   for(int i=0;i<4;i++){
   a=(a-(int)a)*10;
   b=(b-(int)b)*10;
   sum1+=a;
   sum2+=b;
   }
   return sum1>sum2 ? 1:-1;
}
